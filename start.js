const Cluster = require('cluster');
const OS = require('os')
const Utils = require('./utils');

const CPUs = OS.cpus();
const Log = Utils.Log;


if (Cluster.isMaster) {
  Log.log('info', `Starting Master process`, `${process.pid}`);
  Log.debug(`application will fork ${CPUs.length} times`);

  const db = require('./server/database');
  db.initialize();



  function Fork() {
    const W = Cluster.fork();
    W.on('exit', (worker, code, signal) => {
      Log.warn(`process ${W.process.pid} has die, it will be restarted in 1s. Code ${code}; signal ${signal}`);
      process.nextTick( () => {
        setTimeout( function() {
          Fork();
        }, 1000);
      });
    });
  }


  // Fork workers.
  for (let cpu of CPUs ) {
    Fork()
  }


} else {

  // start application
  require('./server/index');

}
