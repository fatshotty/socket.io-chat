const Winston = require('winston');
const Moment = require('moment');

global.DEV = process.env.NODE_ENV !== 'production';

const errorStackTracerFormat = (details) => {
  return Winston.format(info => {
    const lvl = info.level;
    const splat = info[ Symbol.for('splat') ];

    if ( lvl === 'error' && info.stack ) {
      if ( details ) {
        info.message += ` - ${info.stack}`
      }
      delete info.stack;
    } else if ( splat ) {
      const arr = [];
      for( let spl of splat ) {
        const type = typeof spl;
        if ( ['string', 'number', 'undefined', 'boolean'].indexOf( type ) > -1 || spl === null ) {
          arr.push( String(spl) );
        } else {
          arr.push( JSON.stringify(spl) )
        }
      }
      info.message += ` ${arr.join(' ')}`;
    }
    info.level = `${Moment(info.timestamp).format('DD/MM/YYYY HH:mm:ss')} ${info.level}`;
    delete info[ Symbol.for('splat') ];
    delete info[ Symbol.for('info') ];
    delete info.timestamp;
    return info;
  })();
};

const Log = Winston.createLogger({
  level: 'info',
  transports: [
    new Winston.transports.File({ filename: 'server.log' , level: DEV ? 'debug' : 'info',
      format: Winston.format.combine(
        Winston.format.timestamp(),
        errorStackTracerFormat(),
        Winston.format.simple()
      )
    }),
    new Winston.transports.File({ filename: 'server-err.log' , level: 'error',
      format: Winston.format.combine(
        Winston.format.timestamp(),
        errorStackTracerFormat(true),
        Winston.format.simple()
      )
    })
   ]
});


if ( DEV ) {
  Log.add( new Winston.transports.Console({
    format: Winston.format.combine(
      Winston.format.colorize(),
      Winston.format.timestamp(),
      errorStackTracerFormat(true),
      Winston.format.simple()
    )
  }) );
}


module.exports = {Log, Config: process.env};
