const Path = require('path');

module.exports = {


  entry: {
    "app": "./public/page.js"
  },

  devtool: '#eval',
  watch: true,

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      }
    ]
  },
  output: {
    path: `${process.cwd()}/dist/`,
    filename: "[name].js",
    pathinfo: true,
    sourceMapFilename: "[file].js.map"
  }
};
