# Installation

Use git-clone to download project

#### Nodejs version 10+
Use nodejs version 10 or above. NOTE: use `nvm` for managing multiple-version of nodejs


#### Install dependencies

Open shell and type

```sh
npm install
```


#### Assets
Before running program, remind to compile assets. Open shell and type
```sh
npm run watch
```
Once completed, type `CTRL+C` and kill process


#### Configuration
Create `.env` file in order to specify the HTTP server port
```sh
PORT=8080
```
In this way the HTTP server will start listening on port 8080


#### Run program

Open shell and type

```sh
node server/index
```

It starts program without forking, so it will start only one process/thread.


#### Forever running
If you need to continue running program, I suggest use `forever`
