import IO from 'socket.io-client';

const Socket = IO({
  query: {
    tk: window.AUTH,
    feeds: window.CHL
  },
  reconnection: false,
  // parser: MsgParser,
  transports: ['websocket', 'polling']
});

// on reconnection, reset the transports option, as the Websocket
// connection may have failed (caused by proxy, firewall, browser, ...)
// Socket.on('reconnect_attempt', () => {
//   Socket.io.opts.transports = ['polling', 'websocket'];
// });


Socket.on('connect', () => {
  writeLog('(connected)')
});

Socket.on('disconnect', () => {
  writeLog('(disconnected)')
});

Socket.on('error', (err) => {
  console.info('(error occurred)', err);
});

const LogDiv = document.getElementById('logdiv');
const Form = document.getElementById('form_send');
const Feed_el = document.getElementById('feed');
const Message_el = document.getElementById('message');

// Utility: write log in page
function writeLog() {
  const div = document.createElement('div');
  const args = Array.prototype.slice.call(arguments, 0);
  for( let [i, arg] of args.entries() ) {
    if ( typeof arg === 'object' ) {
      args[i] = JSON.stringify(arg);
      arg = args[i];
    }

    const text = document.createTextNode(arg);
    div.appendChild(text);
  }

  div.style.borderBottom = '1px solid gray';
  LogDiv.appendChild( div );
  LogDiv.scrollTop = 10000000;
}


// Sending message
Form.addEventListener('submit', (e) =>{
  // block event in order to avoid browser functionality
  e.preventDefault();

  const channel = Feed_el.value;
  const message = Message_el.value;

  // compress message with msgpack
  Socket.emit('try_send_message', {channel, message} );
  writeLog('SENT message to ', channel, ': ', message);

  // reset form
  Form.reset();
});



Socket.on('new_message', function(packet) {
  // Get message and decode it
  const data = packet; // msgpack.decode(new Uint8Array(packet));
  writeLog('READ message from ', data.from, ' in ', data.channel, ': ', data.message);
});

// writeLog('connecting to', FEEDS);
