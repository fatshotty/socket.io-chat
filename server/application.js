const Express = require('express');
const Session = require('express-session')
const App = Express();
const CORS = require('cors');
const Helmet = require('helmet');
const BodyParser = require('body-parser');
const User = require('../modules/user');
const Package = require('../package.json');
const Messanger = require('./messaging');

const AdminRouter = require('./admin_router');

const Utils = require('../utils');
const Log = Utils.Log;


App.set('trust proxy', 1);
App.use( Helmet() );

App.Locals = Object.assign( App.Locals || {}, { NAME: Package.name, DEV } );

App.use( CORS() );

function initHttpRouters() {

  App.set('view engine', 'pug');

  App.use( Session({
    saveUninitialized: false,
    resave: false,
    secret : 's3Cur3',
    name : 'sid',
  }));

  App.use( BodyParser.json({extended: true}) );
  App.use( BodyParser.urlencoded({extended: true}) );

  App.use( Express.static('dist') );



  /**
   *  Show index page
   */
  App.get('/', (req, res, next) => {
    Log.debug(`GET index`);
    if ( req.session.currentUser ) {
      Log.info('user is logged in', req.session.currentUser.username );
      res.redirect(302, '/messenger');
    } else {
      Log.info('user is not logged in' );
      res.redirect(302, '/users/login');
    }
  });


  // Authentication router
  const AuthenticationRouter = Express.Router();


  // Show login page
  AuthenticationRouter.get('/login', (req, res, next) => {
    Log.debug('GET login');
    res.render('login');
  });

  // login user
  function loginUser(req, res, next) {
    const tw_id = req.body.tw_id;

    Log.debug('POST login', tw_id);

    User.getUser(tw_id).then( (user) => {

      Log.info('User found with tw_id', tw_id);
      Log.debug('User', user);

      req.session.currentUser = user;

      res.redirect(302, '/messenger');

    }, () => {
      next( new Error('User not found') );
    });
  }

  AuthenticationRouter.post('/login', loginUser);

  // login user
  AuthenticationRouter.post('/logout', (req, res, next) => {

    Log.debug('POST logout');

    req.session.currentUser = null;
    delete req.session.currentUser;
    res.redirect(302, '/users/login');
  });

  App.use('/users', AuthenticationRouter);



  // Messenger router
  MessengerRouter = Express.Router();

  MessengerRouter.get('/messenger', (req, res, next) => {
    Log.debug('GET messenger');

    if ( req.session.currentUser && req.session.currentUser.username ) {
      Log.debug('Authentication OK', req.session.currentUser.username);
    } else {
      Log.debug('Authentication KO');
      res.redirect(302, '/users/login');
      return;
    }

    let channels = req.session.currentUser.channels.slice(0);

    Messanger.available_channels().then( (chls) => {
      chls = chls.filter( (c) => {
        return channels.indexOf( c ) == -1;
      })
      channels = channels.concat( chls );

      if ( req.query.chl ) {
        if ( channels.indexOf(req.query.chl) <= -1 ) {
          channels.push( req.query.chl );
        }
      }

      res.render('messenger', {currentUser: req.session.currentUser, auth: req.session.currentUser.tw_id, channels, channel: req.query.chl});
    }, (err) => {
      Log.warn('cannot get list of channels', err);
      res.render('messenger', {currentUser: req.session.currentUser, auth: req.session.currentUser.tw_id, channels, channel: req.query.chl});
    });

  })

  App.use( '/', MessengerRouter);




  /***************
   * ADMIN ACTIONS
   ***************/


   const adminrouter = AdminRouter.initHttp();

   App.use('/admin/actions', adminrouter.ACTIONS );


}

module.exports = {App, initHttpRouters};
