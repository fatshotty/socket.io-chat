const Express = require('express');
const Authentication = require('./authentication');
const Messaging = require('./messaging');
const Utils = require('../utils');

const Log = Utils.Log;


function initHttp() {


  const router_action = Express.Router();


  router_action.use( (req, res, next) => {

    Authentication.authPrivateUrl( req ).then( next, () => {

      Log.error('invalid authentication for private actions');
      res.status(403).end('This action is not available');

    });

  });


  router_action.post('/message', (req, res, next) => {

    Authentication.authUserByReq(req).then( (user) => {

      let json_data = req.body;

      Log.info(`Try to send message to ${json_data.channel} from ${user.username}`);

      Messaging.sendMessage({
        from: user,
        channel: json_data.channel,
        message: json_data.message
      });

      res.status(204).end();

    }, () => {
      res.status(422).end('No user token specified');
    });

  });




  return {
    ACTIONS: router_action
  };


}




module.exports = {initHttp};
