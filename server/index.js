require('dotenv').config();

const Utils = require('../utils')
const Application = require('./application');
const Messanger = require('./messaging');
const Authentication = require('./authentication');
const HTTP = require('http');

const Log = Utils.Log;

const Server = HTTP.createServer( Application.App );


// Enable all routers path
Application.initHttpRouters();

// Enable SocketServer
Messanger.init( Server );


const Cluster = require('cluster');
if (Cluster.isMaster) {
  const db = require('./database');
  db.initialize();
}


/*********************************
 * Add server socket routers API
 *********************************/


/* GET list of all connected clients */
Application.App.get('/admin/users.:format?', (req, res, next) => {

  Authentication.authAdminUserByReq(req).then( (user) => {

    // TODO: get user
    Messanger.client_lists().then( (clients) => {

      res.set('content-type', 'application/json');
      res.status(200).end( JSON.stringify(clients) );

    }, (err) => {
      res.status(422).end(err);
    });

  }, next);

});


/* GET list of all connected clients */
Application.App.get('/channels.:format?', (req, res, next) => {

  Authentication.authUserByReq(req).then( (user) => {

    // TODO: get user
    Messanger.available_channels().then( (rooms) => {

      res.status(200).end( JSON.stringify(rooms) );

    }, (err) => {
      res.status(422).end(err);
    });

  }, next);

});


// Start listening on port
Server.listen(Utils.Config.PORT || 3000, () => {
  Log.info('Process', process.pid, 'Server is listening on port', Utils.Config.PORT);
});


//catches uncaught exceptions
process.on('uncaughtException', function(err) {
  Log.error('** Error ', err);
  process.exit(1);
});

process.on('unhandledRejection', function(reason, p) {
  Log.error('** Promise error ', reason);
});
