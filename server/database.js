const Utils = require('../utils');
// const SQLITE3 = require('sqlite3').verbose();
// const User = require('../modules/user');
const MySql = require('mysql');

// dbase: teamwatch
// user name: maxxam
// password: QmmwIvbaWqXfcDeSafIG
// tabella: users
// id, username, telegram_id, teamwatch_id, role, channels, profile_img


let USERS = [];
try {
  USERS = require('../users.json');
} catch ( e ) {}

const Log = Utils.Log;

class DB {


  get Table() {
    return 'users';
  }

  get Columns() {
    return {
      ID: 'id',
      USERNAME: 'username',
      TELEGRAM_ID: 'telegram_id',
      TEAMWATCH_ID: 'teamwatch_id',
      CHANNELS: 'channels',
      PROFILE_IMG: 'profile_img',
      ROLE: 'role'
    }
  }

  initialize() {
  //   return Log.warn('Database has been disabled');

  //   this.db.serialize(() => {
  //     const query = `
  //       CREATE TABLE IF NOT EXISTS ${this.Table} (
  //         ${this.Columns.EMAIL} TEXT PRIMARY KEY,
  //         ${this.Columns.PWD} TEXT NOT NULL,
  //         ${this.Columns.ROLE} TEXT NOT NULL
  //       );
  //     `;
  //     Log.debug(query);
  //     this.db.run( query );

  //     if ( DEV ) {
  //       Log.debug('try to insert dump: ', USERS.length, 'users');
  //       for ( let u of USERS ) {
  //         Log.info('dump for', u);
  //         this.createUser(u).then( ()=>{}, ()=>{});
  //       }
  //     }

  //   });
  }

  constructor() {

    Log.info(`connecting to MySQL ${Utils.Config.MYSQL_SERVER}:${Utils.Config.MYSQL_DB}`);

    this.db = MySql.createConnection({
      host     : Utils.Config.MYSQL_SERVER,
      user     : Utils.Config.MYSQL_USER,
      password : Utils.Config.MYSQL_PWD,
      database : Utils.Config.MYSQL_DB
    });

    this.db.connect( (err) => {
      if (err) {
        Log.error('error connecting: ' + err.stack);
        return;
      }

      Log.info('connected as id ' + this.db.threadId );
    });

    process.on('exit', () => {
      this.db.end();
    });

  }


  // createUser( user ) {
  //   const query = [
  //     `INSERT INTO ${this.Table} (${this.Columns.USERNAME}, ${this.Columns.TELEGRAM_ID}, ${this.Columns.TEAMWATCH_ID}, ${this.Columns.CHANNELS}, ${this.Columns.PROFILE_IMG}, ${this.Columns.ROLE})`,
  //     `values ( ?, ?, ?, ?, ?, ? )`
  //   ];

  //   let q_str = query.join(' ');

  //   Log.debug( q_str );

  //   return new Promise( (resolve, reject) => {
  //     // const stmt = this.db.prepare( q_str );
  //     // stmt.run( `${user.email}`, `${user.password}`, user.role, (err, row) => {
  //     //   if ( err ) return reject(err);
  //     //   this.db.each('select last_insert_rowid() as id', (err, row) => {
  //     //     // row.id
  //     //     resolve(row);
  //     //   });
  //     // });

  //     let inserts = [user.username, user.ID, user.tw_id, user.channels, user.profile_img, user.role];
  //     q_str = mysql.format(q_str, inserts);
  //     Log.debug( q_str );
  //     this.db.query(sql, (error, results, fields) => {
  //       if (error) return reject(error);
  //       user._uuid = results.insertId;
  //       resolve({id: results.insertId});
  //     });
  //   });

  // }

  // updateUser( user ) {
  //   const query = [
  //     `UPDATE ${this.Table}`,
  //     ` SET ${this.Columns.EMAIL}=(?),`,
  //     ` SET ${this.Columns.PWD}=(?),`
  //     ` SET ${this.Columns.ROLE}=(?)`
  //     `WHERE ${this.Columns.ID}=${user.ID}`
  //   ];

  //   const q_str = query.join('\n');

  //   Log.debug( q_str );

  //   return new Promise( (resolve, reject) => {
  //     const stmt = this.db.prepare( q_str );
  //     stmt.run( `${user.email}`, `${user.password}`, user.role, (err, row) => {
  //       if ( err ) return reject( err );
  //       resolve(row);
  //     });
  //   });

  // }


  getUser(token) {

    // let user = USERS.filter( (u) => {
    //   return u.tw_id == token;
    // });

    // return user && user.length ? Promise.resolve( user[0] ) : Promise.reject();

    const query = [
      `SELECT ${this.Columns.ID} , ${this.Columns.USERNAME} , ${this.Columns.TELEGRAM_ID} , ${this.Columns.TEAMWATCH_ID} , ${this.Columns.CHANNELS} , ${this.Columns.PROFILE_IMG} , ${this.Columns.ROLE}`,
      `FROM ${this.Table}`
    ];
    if ( typeof token == 'number' ) {
      // query by id
      query.push( 'WHERE' );
      query.push( `${this.Columns.ID} = ?`);
    } else {
      // query by email/password
      query.push( 'WHERE' );
      query.push(`${this.Columns.TEAMWATCH_ID} = ?` );
    }

    let q_str = query.join(' ');

    q_str = this.db.format(q_str, [token] );
    Log.debug(q_str);

    return new Promise( (resolve, reject) => {
      this.db.query( q_str, function(error, rows, fields) {
        if ( error ) reject(error);
        else resolve( rows[0] );
      });
    });
  }

}


module.exports = new DB();
