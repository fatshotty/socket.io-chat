const Authentication = require('./authentication')
const SocketIO = require('socket.io');
const Utils = require('../utils');


const Log = Utils.Log;

const FEEDS_QUERY_PARAM = 'feeds';


let IO = null;

function INIT( httpServer ) {

  IO = SocketIO(httpServer, {
    // parser: MsgParser
  });


  if ( Utils.Config.USE_REDIS ) {
    const RedisAdapter = require('socket.io-redis');
    IO.adapter( RedisAdapter({ host: Utils.Config.REDIS_HOST || 'localhost', port: parseInt(Utils.Config.REDIS_PORT || 6379, 10) }) );
  }


  /**
   * Check authentication
   * see https://github.com/socketio/socket.io/blob/9c1e73c752aec63f48b511330a506d037783d897/docs/API.md#sockethandshake
   */
  IO.use( (client, next) => {
    // This is the First filter application

    let METHOD = 'authUserByToken';
    let PARAM = '';

    // Check authentication
    const handshake = client.handshake;
    if ( handshake.query[Authentication.TokenQueryParam] ) {
      // check authentication via querystring
      PARAM = handshake.query[Authentication.TokenQueryParam];

    } else if ( handshake.headers[Authentication.TokenHeaderParam] ) {
      // check authentication via headers
      PARAM = handshake.headers[Authentication.TokenHeaderParam];

    }

    if ( METHOD && PARAM ) {
      Authentication[METHOD]( PARAM ).then( (user) => {
        handshake.currentUser = user;
        next();
      }, () => {
        Log.warn('Invalid user token', METHOD, PARAM);
        next( new Error('Auth failed') );
      });
    } else {
      Log.warn('Invalid authentication method', METHOD, PARAM);
      next( new Error('Auth failed') );
    }

  });


  // Handle all connection
  IO.on('connection', (client) => {
    // Client has been correctly authenticated
    const currentUser = client.handshake.currentUser;
    const queryString = client.handshake.query;

    const STR_LOG = `${currentUser.username} (${client.id})`;

    let ReadChannels = [], WriteChannels = [], UserEmail = 'unknown';
    ReadChannels = ReadChannels.concat( currentUser.channels );
    // WriteChannels = WriteChannels.concat( currentUser.channels );
    UserEmail = currentUser.username;

    // Join into personal channel
    ReadChannels.unshift( currentUser.tw_id );

    Log.info('Try to join client to mandatory rooms', ReadChannels);
    for ( let room of ReadChannels ) {
      joinClient( room, true, false );
    }

    if ( queryString[FEEDS_QUERY_PARAM] ) {
      // User wants to join to a feed room
      joinClient( queryString[FEEDS_QUERY_PARAM], true, true );
    }


    client.on('disconnect', function() {
      Log.warn(STR_LOG, ' - DISCONNECTED !!');
    });


    /**
     * A client wants to sending a message
     * @param feed: String representing the 'feed'
     * @param message: String representing the 'message' to send
     */
    client.on('try_send_message', (packet) => {
      if ( !packet.channel ) {
        // invalid packet
        Log.warn(STR_LOG, 'invalid packet', packet);
        // DO NOTHING
        return;
      }

      const decodedData = packet;
      const channel = decodedData.channel;
      const message = decodedData.message;

      // TODO: can user send message to this channel?

      const data_to_send = {
        from: currentUser,
        channel: channel,
        message: message,
        type: '#channel'
      };

      Log.debug(STR_LOG, 'is trying to send message', data_to_send);

      sendMessage( data_to_send, null, client );
    });



    // Utility function
    function joinClient(feed_room, special, exit) {
      feed_room = feed_room.trim();
      let channels = feed_room.split(',');
      channels = channels.map( (c) => {
        let m = c.match(/\w+/);
        return m ? m[0] : '';
      }).filter( (c) => {
        let num = Number(c);
        let ok = isNaN(c) || !!c;
        // avoid number, empty string, undefined or null
        if ( !ok ) {
          Log.warn(STR_LOG, 'User wants to join to a non-valid channel', c);
        }
        return !!c;
      });

      Log.info(STR_LOG, 'User will be joined to \'', channels.join(', '), '\'');

      let cont = true;
      for ( let chl of channels ) {
        if ( cont ) {
          client.join( chl, (err) => {
            if ( !err ) {
              Log.debug( STR_LOG, 'has been joined to', chl);
            } else {
              Log.warn(STR_LOG, 'cannot be joined into', chl, 'due to', err);
              if ( exit ) {
                // skip all other channels
                Log.error(STR_LOG, 'won\'t be joined and will be disconnected');
                process.nextTick( () => {
                  client.disconnect();
                });
                cont = false;
              }
            }
          });
        }
      }
    }

  });
}


/**
 * Returns list of connected users
 */
function clientLists(with_sock) {

  // TODO: be sure it still work with Redis
  // TODO: add login time

  return new Promise( (resolve, reject) => {

    IO.clients( (error, clients) => {

      if ( error ) {
        return reject(error);
      }

      let result = {};

      for ( let sock_id of clients ) {
        const sock = IO.sockets.connected[sock_id];
        if ( sock.handshake && sock.handshake.currentUser ) {
          let user = sock.handshake.currentUser;

          let user_data = result[ user.username ];
          if ( !user_data ) {
            user_data = result[ user.username ] = Object.assign({}, sock.handshake.currentUser.toJSON());
            user_data.channels = [];
          }

          // calculate rooms/channels for each client
          let rs = Object.keys(sock.rooms);
          for ( let r of rs ) {
            if ( r !== sock_id && r !== user_data.tw_id && user_data.channels.indexOf( r ) <= 0 ) {
              user_data.channels.push( r );
            }
          }

          if ( with_sock ) {
            user_data.socks = user_data.socks || (user_data.socks = []);
            user_data.socks.push( sock );
          }

        } else {
          Log.warn('Socket has no user associated to it', sock_id);
        }
      }

      let response = [];

      // refactor response
      let keys = Object.keys(result);
      for ( let k of keys ) {
        let user_data = result[ k ];
        response.push( user_data );
      }


      /*
       * return [ {user_data}, {user_data} ]
       */
      resolve(response);
    });
  });

}


function availableChannels() {

  return clientLists().then( (clients) => {

    let channels = [];
    for ( let client of clients ) {

      let rooms = client.channels;
      rooms = rooms.filter( (r) => {
        return channels.indexOf(r) == -1;
      });

      channels = channels.concat( rooms );
    }

    return channels;
  });

}



function sendMessage(data, event, client) {
  Log.info('SOCKET Sending message');

  if ( !data || !data.channel ) {
    Log.error('invalid packet to send', data);
    return
  }

  let dest = IO.sockets;

  if ( client ) {
    dest = client
  }

  // let data_to_send = {
  //   from: data.from,
  //   channel: data.channel,
  //   message: data.message
  // };

  // TODO
  dest.to(data.channel).emit(event || 'new_message', data);
  Log.info('SOCKET message sent:', data);
}


module.exports = {
  init: INIT,
  client_lists: clientLists,
  available_channels: availableChannels,
  sendMessage: sendMessage
};
