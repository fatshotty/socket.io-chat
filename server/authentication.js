const Utils = require('../utils');
const User = require('../modules/user');

const Log = Utils.Log;


const PRIVATE_ACTIONS_TOKEN = 't34mw4tch';

const TOKEN_QUERY_PARAM = 'tk';
const TOKEN_HEADER_PARAM = 'x-auth-token';

class Auth {

  get TokenQueryParam() {
    return 'tk';
  }

  get TokenHeaderParam() {
    return 'x-auth-token';
  }

  constructor() {

    this.options = {

    };


  }


  auth(email, password) {
    return new Promise( (resolve, reject) => {
      const user = {};

      resolve(user);

    });
  }

  authAdminUserByReq(req) {
    return new Promise( (resolve, reject) => {
      this.authUserByReq(req).then( (user) => {

        if ( user.role == User.Roles.Admin ) {
          resolve(user);
        } else {
          reject( 'Access denied because you are not Admin' );
        }

      }, reject)
    });
  }

  authUserByReq(req) {

    // TODO: check user
    return new Promise( (resolve, reject) => {

      Log.debug('try to authenticate user');

      let method = 'authUserByToken';
      let param = '';

      if ( req.headers && req.headers[this.TokenHeaderParam] ) {
        Log.info('using header');

        param = req.headers[this.TokenHeaderParam];

      } else if ( req.query && req.query[this.TokenQueryParam] ) {

        Log.info('using token query param');

        param = req.query[this.TokenQueryParam];

      }

      if ( method && param ) {
        this[ method ]( param ).then( (usr) => {
          Log.info('user is valid');
          resolve( usr );
        }, () => {
          reject( 'invalid token or auth');
        });
      } else {
        Log.warn('invalid authentication parameters');
        reject( 'Invalid authentication');
      }

    });
  }

  authUserByToken( token ) {
    Log.info('Try auth user by token', token);

    // let buff = Buffer.from(token, 'base64');
    // let auth_str = buff.toString('ascii');
    // let auth = auth_str.split(':');
    // const email = auth[0];
    // const password = auth[1];

    Log.debug('decoded token', token);

    return User.getUser( token );
  }


  authPrivateUrl( req ) {

    return new Promise( (resolve, reject) => {

      let headers = req.headers || {};
      let authorization = headers.authorization || '';

      Log.debug('Try auth privately', authorization);

      let buff = Buffer.from(authorization, 'base64');
      let auth_str = buff.toString('ascii');

      if ( auth_str == PRIVATE_ACTIONS_TOKEN ) {
        Log.info('PrivateToken correctly got');
        resolve();
      } else {
        Log.warn('PrivateToken doesn\'t match!!', `'${auth_str}'`);
        reject( new Error('invalid authorization') );
      }
    });
  }


}


module.exports = new Auth();
