const DB = require('../server/database');

const UserRole = 'User';
const AdminRole = 'Admin';

class User {

  static get Roles() {
    return {
      User: UserRole,
      Admin: AdminRole
    };
  }

  constructor(data) {

    if ( data ) {
      this._uuid = data.id;
      this._username = data.username;
      this._telegram_id = data.telegram_id;
      this._tw_id = data.teamwatch_id;
      this._channels = (data.channels || '').split(',').map( (c) => c.trim() );
      this._profile_img = data.profile_img;
      this._role = data.role;
    }
    this._role = this._role || User.Roles.User;
  }

  get uuid() {
    return this._uuid
  }

  get username() {
    return this._username;
  }
  set username(value) {
    this._username = value;
  }

  get tw_id() {
    return this._tw_id;
  }
  set tw_id(value) {
    this._tw_id = value;
  }

  get role() {
    return this._role;
  }
  set role(value) {
    this._role = value;
  }

  get ID() {
    return this._telegram_id;
  }
  set ID(value) {
    this._telegram_id = value;
  }

  get profile_img() {
    return this._profile_img;
  }

  get channels() {
    return this._channels;
  }


  toJSON(skip_details) {
    const data = {
      uuid: this.uuid,
      username: this.username,
      tw_id: this._tw_id,
      channels: this.channels,
      profile_img: this.profile_img,
      role: this.role
    };
    return data;
  }

  toString(skip_details) {
    return `(${this.role}): ${this.username}`;
  }


  static getUser(token) {

    return new Promise( (resolve, reject) => {
      DB.getUser(token).then( (data) => {
        if ( data ) {
          resolve( new User( data ) );
        } else {
          reject();
        }
      }, reject );
    });

  }

  // static getAll() {

  //   return DB.getAllUsers().then( (data) => {
  //     const arr = [];
  //     for( let u of data ) {
  //       arr.push( new User( u ) );
  //     }
  //     return arr;
  //   });

  // }


  // save() {

  //   const method = this.ID ? 'updateUser' : 'createUser';
  //   const db_action = DB[ method ];

  //   return db_action.call(DB, this ).then( (data) => {
  //     this._id = data.id;
  //     return this;
  //   });
  // }


}


module.exports = User;
